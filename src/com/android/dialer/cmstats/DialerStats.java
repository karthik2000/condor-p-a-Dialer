/*
 * Copyright (C) 2014 The CyanogenMod Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.dialer.cmstats;

import android.content.Context;
import android.content.Intent;
import android.os.UserHandle;

import com.android.dialer.R;

public class DialerStats {

    public static final String TRACKING_ID = "tracking_id";

    public static final class Fields {
        public static final String EVENT_CATEGORY = "category";
        public static final String EVENT_ACTION = "action";
        public static final String EVENT_LABEL = "label";
        public static final String EVENT_VALUE = "value";
    }

    public static final class Categories {
        public static final String APP_LAUNCH = "app_launch";
        public static final String BUTTON_EVENT = "button_event";
        public static final String INITIATE_CALL = "initiate_call";
        public static final String INCALL_CONTACT_IMAGE = "incall_contact_image";
        public static final String DETAILS_CONTACT_IMAGE = "details_contact_image";
    }

    public static void sendEvent(Context context, String category, String action,
                                 String label, String value) {

            return;

    }

    public static void sendEvent(Context context, String category, String action) {
        sendEvent(context, category, action, null, null);
    }
}
